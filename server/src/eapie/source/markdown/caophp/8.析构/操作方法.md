<blockquote class="info"><p>注册最后执行的方法，自毁时执行，该操作慎重使用。</p></blockquote>

**下面的情况会调用注册的方法：**

1. 当页面被用户强制停止时
2. 当程序代码运行超时时
3. 当php代码执行完成时，代码执行存在异常和错误、警告


##### 全局赋值函数：
~~~
destruct([mixed $identify][,closure $closure][,array $args][,bool $coverage]);
~~~

##### 继承框架类：
~~~
framework\cao::destruct([mixed $identify][,closure $closure][,array $args][,bool $coverage]);
~~~


| 参数类型  |  参数名称  |  参数备注  |   是否必须   |  传参顺序   |
| --- | --- | --- | --- | --- |
|  string/int/float  |  $identify  |  标识  |  否  |  mixed[0]  |
|  closure  |  $closure  |  闭包函数，业务处理  |  否  | closure[0] |
|  array  |  $args  |  闭包函数的参数组 |  否  | array[0] |
|  bool  |  $coverage  |  是否覆盖已存在标识  |  否  | bool[0]  |

- $args 如array(参数1,参数2,...)，是一个索引数组。在最后执行时传给$closure闭包函数，如$closure = function(参数1,参数2,...){} 
- $coverage 为true覆盖则已存在标识，默认false则不覆盖。
- 在定义操作时，成功返回true，失败(比如存在定义非覆盖情况下)返回false
- 在指定标识获取时，存在则已，不存在返回NULL
- 在获取全部定义列表时，如果不存在定义项，则返回一个空数组。