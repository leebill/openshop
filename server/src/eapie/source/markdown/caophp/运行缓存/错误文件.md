错误缓存主要是保存当天错误信息的文件：

```
$error_config = self::config('error');

//下面是错误目录文件定义规则	
CACHE_PATH.
DIRECTORY_SEPARATOR.
$error_config['cache_folder_name'].
DIRECTORY_SEPARATOR.
date('Ymd').'.log';//根据每天来统计

//例如
cache\error\20180311.log
```
