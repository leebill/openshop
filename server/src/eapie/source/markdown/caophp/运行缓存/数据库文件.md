#### 数据库的缓存目录
```
$db_config = config('db');
//这两个参数是关系到目录名称，可以自定义
$db_config['type'];
$db_config['cache_folder_name'];
	
//默认
CACHE_PATH.DIRECTORY_SEPARATOR.
$db_config['cache_folder_name'].
DIRECTORY_SEPARATOR.
$db_config['type'];

//例如：
根目录/cache/db/mysql
```


---

#### 会话文件缓存
数据库的会话缓存目录，储存生成创建会话表的SQL文件记录。

```
//文件命名规则是：
./DIRECTORY_SEPARATOR.'session'.parent::cmd(array(22), 'random autoincrement').'.sql'

//例如
cache/db/mysql/session/c2c60f60cbdcdfd83f833215208400251036.sql
```

---

#### 锁文件缓存

锁会根据当前标识连接的“主机”、“端口”的不同而产生多个文件，该类型文件储存了锁信息。该文件内容是一个serialize格式化的数据。

```
./DIRECTORY_SEPARATOR.'lock'.md5($resource['config']['host'].':'.$resource['config']['port']).'.serialize';

//例如
cache/db/mysql/lock/c2c60f60cbdcdfd83f833215208400251036.serialize
```

---


#### 事务缓存

主要储存事务操作产生的数据备份，以及数据回滚产生的日志文件。


```
//回滚日志，根据每天来统计
./DIRECTORY_SEPARATOR.'work'.'work_rollback_'.date('Ymd').'.log';

//备份文件
./DIRECTORY_SEPARATOR.'work'.parent::cmd(array(22), 'random autoincrement').'.sql';
```
