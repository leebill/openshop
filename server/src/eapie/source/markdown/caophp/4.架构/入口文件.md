<blockquote class="info"><p>caophp 框架使用单一入口访问，所有的请求都是通过入口文件完成的。无论从安全性还是方法调用及文件加载方面都带来了很高的便捷性。</p></blockquote>

* 创建一个 index.php 入口文件。代码如下：
~~~
//引入框架类文件
include 'framework/cao.php';
//配置信息
$config = array(
    'error'=>array(
            'start'=> true,
            'catch'=> false
    ),
    'db' => array(
		'method_log' => true,	//是否记录method执行信息。
		'query_log'=>true,	//是否记录query语句信息。
	),
    'template'	=> array(
        'html_cache_start' => false,	//是否开启html缓存。
    )
);
//初始化项目
framework\cao::install($config, function(){
	//...
	});
~~~