[TOC]
## 初始化框架方法
```
//参数为空则能获取初始化框架信息
av.framework({
    //框架对象参数
});
```
## 初始化框架参数
|  键   |  类型  | 默认值| 描述| 
| --- | --- | --- |--- |
|  version |  String | "0.0.1" | 版本号，会在引入文件和导出文件的地址上加上版本号信息   |
|  includeSelector   |  String/HTMLElement | “action” | 默认引入文件的父元素标签名称/节点对象   |
|  renderSelector |  String/HTMLElement | "view"  | 默认渲染的父元素标签名称/节点对象   |
|  includeTime |  Bool | false  | 引入文件是否加上时间参数。true会加上，false不加上。目的是在引入文件地址上加上当前时间参数，防止缓存   |
|  exportTime |  Bool | false  | 导出文件是否加上时间参数。true会加上，false不加上。目的是在导出文件地址上加上当前时间参数，防止缓存   |
|  historyPageMaximum |  Int| 10 | 浏览历史，页面变更列表最大值   |
|  historyRouterMaximum |  Int| 10 | 浏览历史，锚点路由变更列表记录最大值   |
|  debug |  Bool | false  | 调试模式，在console面板上打印框架调试信息   |

## 初始化框架示例
可以创建一个初始化入口文件，如“main.js”：
```
//初始化框架
av.framework({
    //版本号
    version: '1.1.2',
    //引入文件是否加上时间戳参数
    includeTime: true,
    //导入文件是否加上时间戳参数
    exportTime: true,
    //调试模式
    debug: false,
    //默认引入文件的父元素标签名称
    includeSelector: 'action',
    //默认渲染的父元素标签名称
    renderSelector: 'view',    
    //路由配置
    page:{
        //两个路径都指定到'page-home'工程
        '':['page-home', 'home.js'],
        '/':["page-home", 'home.js'],
        // 'path路径':['工程标识', '动态引入工程文件'],
    },
    //公共事件
    event: {
        //页面开始加载时
        pageStart:function(){},
        //页面完成加载时
        pageEnd:function(){},
	//页面加载有错误时
        error:function(message){},
        //当页面不存在时
        pageNotFound:function(href){},
        //更多事件请查看公共事件讲解
    }
}).run();//以默认方式运行
```


## 安装
### 直接用script标签引入av.js
在首页 “.html”文件（如index.html、default.html）中，通过如下方式引入 av.js：
```
<script src="../av.min.js" charset="UTF-8"></script>
```
### 装载入口文件
引入初始化框架的配置文件，一般会放在 includeSelector 配置的筛选器的节点中：
```
<body>
    <view></view>
    <action>
        <script src="src/main.js?v.1.1" charset="UTF-8"></script>
    </action>
</body>
```






